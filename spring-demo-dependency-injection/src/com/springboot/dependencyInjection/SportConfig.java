package com.springboot.dependencyInjection;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.springboot.dependencyInjection")
public class SportConfig {

}
