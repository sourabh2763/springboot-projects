package com.springboot.dependencyInjection;

import org.springframework.stereotype.Component;

@Component
public class SadFortuneService implements FortuneService {

	@Override
	public String getFortune() {
		
		return "Bad Day";
	}

}
