package com.springboot.dependencyInjection;

import org.springframework.stereotype.Component;

@Component
public class HappyFortuneService implements FortuneService {

	@Override
	public String getFortune() {
		
		return " My Lucky Day";
	}

}
