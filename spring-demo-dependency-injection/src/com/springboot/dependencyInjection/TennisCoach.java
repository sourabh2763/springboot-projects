package com.springboot.dependencyInjection;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
//@Scope("prototype")
public class TennisCoach implements Coach {
	
//	@Autowired
//	@Qualifier("sadFortuneService")
	private FortuneService fortuneService;
	
	
	@Autowired
	public TennisCoach(@Qualifier("happyFortuneService") FortuneService theFortuneService) {
		System.out.println("In Constructor");
		fortuneService =theFortuneService;
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
	@Override
	public String getDailyWorkout() {
		return "Do Backflip 3 Times";
	}
	
	@PostConstruct
	public void StartUpStuff() {
		System.out.println("Inside the startup method");
	}
	
	@PreDestroy
	public void CleanUpStuff() {
		System.out.println("Inside the cleanup method");
	}
	
	public static void DoOtherStuff() {
		System.out.println("Nothing to do...! Lets Practice");
	}
	

}
