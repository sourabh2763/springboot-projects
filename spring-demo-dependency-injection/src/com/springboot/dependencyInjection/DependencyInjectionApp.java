package com.springboot.dependencyInjection;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DependencyInjectionApp {

	public static void main(String[] args) {
		//Read
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		// get bean
		Coach theCoach= context.getBean("tennisCoach",Coach.class);
		
		
		System.out.println(theCoach.getDailyWorkout());
		
		
		System.out.println(theCoach.getDailyFortune());
		
		TennisCoach.DoOtherStuff();
	
		context.close();

	}

}
