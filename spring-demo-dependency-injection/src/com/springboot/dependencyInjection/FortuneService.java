package com.springboot.dependencyInjection;

public interface FortuneService {

	public String getFortune() ;
}
