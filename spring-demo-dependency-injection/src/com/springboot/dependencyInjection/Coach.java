package com.springboot.dependencyInjection;

public interface Coach {

	public String getDailyWorkout();
	public String getDailyFortune();
}
